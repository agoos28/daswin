const pkg = require('./package')

module.exports = {
  mode: 'universal',
  head: {
    title: 'Daswin Office Tower',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/images/components/logo-daswin-black.png' }
    ],
    script: [
      // { src: '/js/jquery.3.1.1.min.js' },
      { src: 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js' },
      // { src: 'https://code.jquery.com/jquery-1.10.2.js' },
      { src: '/js/index.js' } // script for jquery library customize
    ]
  },
  loading: { color: '#fff' },
  css: [
    { src: '~assets/css/icons.css' },
    '@/assets/scss/main.scss'
  ],
  plugins: ['~/plugins/vue-scrollto'],
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt'
    // 'nuxt-sass-resources-loader',
    // [
    //   'assets/scss/main.scss',
    // ]
  ],
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },
  build: {
    vendor: [],
    plugins: [],
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      
    }
  }
}
